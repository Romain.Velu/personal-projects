package fr.eurecom.introduceme;

import android.app.Dialog;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class HisOrHerInfo extends AppCompatActivity {
    SharedPreferences preferences;


    CheckBox checkboxfName,checkboxName,checkboxFacebook,checkboxTwitter,checkboxInstagram,checkboxSnapchat,checkboxLinkedin,checkboxMail,checkboxPhone,checkboxAddress,checkboxCompany,checkboxJob;
    final Context context = this;
    private ContactViewModel mContactViewModel;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.hisorherinfo_layout);  //instatiating the myinfo layout

        mContactViewModel = ViewModelProviders.of(this).get(ContactViewModel.class);

        int ID = getIntent().getIntExtra("the contact ID", 0);

        mContactViewModel.mRepository.mContactDao.loadById(ID).observe( (LifecycleOwner) this, new Observer<Contact>() {
            @Override
            public void onChanged(@Nullable final Contact htry) {
                if(htry == null ) {
                    Toast.makeText(getApplicationContext(),"wait...",Toast.LENGTH_LONG).show();
                }else{
                    TextView new_fname = findViewById(R.id.textViewfName);
                    new_fname.setText(htry.getFirstName());

                    TextView new_name = findViewById(R.id.textViewName);
                    new_name.setText(htry.getLastName());

                    TextView new_facebook = findViewById(R.id.textViewFacebook);
                    new_facebook.setText(htry.getFacebookAcc());

                    TextView new_twitter = findViewById(R.id.textViewTwitter);
                    new_twitter.setText(htry.getTwitterAcc());

                    TextView new_instagram = findViewById(R.id.textViewInstagram);
                    new_instagram.setText(htry.getInstaAcc());

                    TextView new_snapchat = findViewById(R.id.textViewSnapchat);
                    new_snapchat.setText(htry.getSnapchatAccount());

                    TextView new_linkedin = findViewById(R.id.textViewLinkedin);
                    new_linkedin.setText(htry.getLinkedInAcc()) ;

                    TextView new_mail = findViewById(R.id.textViewMail);
                    new_mail.setText(htry.getMailAddress());

                    TextView new_phone = findViewById(R.id.textViewPhone);
                    new_phone.setText("0"+String.valueOf(htry.getMobileNumber()));

                    TextView new_address = findViewById(R.id.textViewAddress);
                    new_address.setText(htry.getLivingAddress());

                    TextView new_company = findViewById(R.id.textViewCompany);
                    new_company.setText(htry.getCompany());

                    TextView new_job = findViewById(R.id.textViewJob);
                    new_job.setText(htry.getJob());

                    TextView new_metat = findViewById(R.id.textViewMetAt);
                    new_metat.setText("Met at : "+ htry.getMetLocation());

                    TextView new_meton = findViewById(R.id.textViewMetOn);
                    new_meton.setText(htry.getMetDate());
                }
            }
        });

    }
}

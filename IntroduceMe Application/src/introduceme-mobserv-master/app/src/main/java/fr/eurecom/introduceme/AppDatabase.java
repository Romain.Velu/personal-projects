package fr.eurecom.introduceme;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

@Database(entities = {Contact.class}, version = 3)
public abstract class AppDatabase extends RoomDatabase {

    public abstract ContactDao contactDao();

    private static volatile AppDatabase INSTANCE;

    static AppDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (AppDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            AppDatabase.class, "contact_database")
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    private static RoomDatabase.Callback sRoomDatabaseCallback =
            new RoomDatabase.Callback(){

                @Override
                public void onOpen (@NonNull SupportSQLiteDatabase db){
                    super.onOpen(db);
                    new PopulateDbAsync(INSTANCE).execute();
                }
            };


    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

        private final ContactDao mDao;

        PopulateDbAsync(AppDatabase db) {
            mDao = db.contactDao();
        }

        @Override
        protected Void doInBackground(final Void... params) {
            //mDao.deleteAll();
            if (mDao.getAll2().size() > 0) {
                Log.d("A", "mDao.getAll().getValue().size() > 0");
                return null;
            } else {
                Log.d("A", "else");
                Contact contact0 = new Contact();
                contact0.setFirstName("Me");
                contact0.setLastName("Myself");
                contact0.setMobileNumber(666666666);
                contact0.setIsMe(1);
                mDao.insert(contact0);
                Contact contact1 = new Contact();
                contact1.setFirstName("Hermione");
                contact1.setLastName("Granger");
                contact1.setMobileNumber(612345678);
                contact1.setMetDate("12-12-2012");
                contact1.setMetLocation("Hogwarts");
                mDao.insert(contact1);
                Contact contact2 = new Contact();
                contact2.setFirstName("Harry");
                contact2.setLastName("Potter");
                contact2.setMobileNumber(612345677);
                contact2.setMetDate("06-06-2006");
                contact2.setMetLocation("Hogwarts");
                mDao.insert(contact2);
                Contact contact3 = new Contact();
                contact3.setFirstName("Ron");
                contact3.setLastName("Weasley");
                contact3.setMobileNumber(612345676);
                contact3.setMetDate("03-03-2003");
                contact3.setMetLocation("Hogwarts");
                mDao.insert(contact3);
                Contact contact4 = new Contact();
                contact4.setFirstName("Ginny");
                contact4.setLastName("Weasley");
                contact4.setMobileNumber(612345675);
                contact4.setMetDate("01-01-2001");
                contact4.setMetLocation("Hogwarts");
                mDao.insert(contact4);
                return null;
            }
        }
    }
}

package fr.eurecom.introduceme;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import java.util.List;

public class MyContactsActivty extends AppCompatActivity {
    private ContactViewModel mContactViewModel;
    public static final int NEW_CONTACT_ACTIVITY_REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i("Info","MY CONTACTS PRESSED");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_contacts_activty);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        RecyclerView recyclerView = findViewById(R.id.recyclerview);
        final ContactListAdapter adapter = new ContactListAdapter(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        mContactViewModel = ViewModelProviders.of(this).get(ContactViewModel.class);
        if (mContactViewModel.getAll() == null) {
            Toast.makeText(getApplicationContext(),"something",Toast.LENGTH_LONG).show();
        }

        mContactViewModel.getAll().observe(this, new Observer<List<Contact>>() {
            @Override
            public void onChanged(@Nullable final List<Contact> words) {
                // Update the cached copy of the words in the adapter.
                adapter.setContact(words);
            }
        });
    }

    public void DegustDetail2(View view){
        Toast.makeText(getApplicationContext(), ContactListAdapter.IDcurrentContact.toString(), Toast.LENGTH_LONG).show();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode != NEW_CONTACT_ACTIVITY_REQUEST_CODE || resultCode != RESULT_OK) {
            Toast.makeText(
                    getApplicationContext(),
                    R.string.empty_not_saved,
                    Toast.LENGTH_LONG).show();
        }
    }


}

package fr.eurecom.introduceme;

import android.content.DialogInterface;
import android.content.Intent;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.NfcEvent;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.linkedin.platform.APIHelper;
import com.linkedin.platform.LISessionManager;
import com.linkedin.platform.errors.LIApiError;
import com.linkedin.platform.errors.LIAuthError;
import com.linkedin.platform.listeners.ApiListener;
import com.linkedin.platform.listeners.ApiResponse;
import com.linkedin.platform.listeners.AuthListener;
import com.linkedin.platform.utils.Scope;

import java.nio.charset.Charset;
import java.util.ArrayList;

public class FragmentSend extends Fragment implements NfcAdapter.OnNdefPushCompleteCallback,
        NfcAdapter.CreateNdefMessageCallback  {
    private NfcAdapter mNfcAdapter;
    android.content.SharedPreferences preferences;

    public MainActivity mainActivity;

    //The array lists to hold our messages
    public ArrayList<String> messagesToSendArray = new ArrayList<>();
    public ArrayList<String> messagesReceivedArray = new ArrayList<>();

    //Save our Array Lists of Messages for if the user navigates away
    @Override
    public void onSaveInstanceState(@NonNull Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putStringArrayList("messagesToSend", messagesToSendArray);
        savedInstanceState.putStringArrayList("lastMessagesReceived",messagesReceivedArray);
    }

    //Load our Array Lists of Messages for when the user navigates back
    public void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        messagesToSendArray = savedInstanceState.getStringArrayList("messagesToSend");
        messagesReceivedArray = savedInstanceState.getStringArrayList("lastMessagesReceived");
    }



    @Override
    public void onNdefPushComplete(NfcEvent event) {

    }

    @Override
    public NdefMessage createNdefMessage(NfcEvent event) {
        //This will be called when another NFC capable device is detected.
        if (messagesToSendArray.size() == 0) {
            return null;
        }
        //We'll write the createRecords() method in just a moment
        NdefRecord[] recordsToAttach = createRecords();
        //When creating an NdefMessage we need to provide an NdefRecord[]
        return new NdefMessage(recordsToAttach);
    }

    public NdefRecord[] createRecords() {
        NdefRecord[] records = new NdefRecord[messagesToSendArray.size() + 1];
        //To Create Messages Manually if API is less than
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            for (int i = 0; i < messagesToSendArray.size(); i++){
                byte[] payload = messagesToSendArray.get(i).
                        getBytes(Charset.forName("UTF-8"));
                NdefRecord record = new NdefRecord(
                        NdefRecord.TNF_WELL_KNOWN,      //Our 3-bit Type name format
                        NdefRecord.RTD_TEXT,            //Description of our payload
                        new byte[0],                    //The optional id for our Record
                        payload);                       //Our payload for the Record

                records[i] = record;
            }
        }
        //Api is high enough that we can use createMime, which is preferred.
        else {
            for (int i = 0; i < messagesToSendArray.size(); i++){
                byte[] payload = messagesToSendArray.get(i).
                        getBytes(Charset.forName("UTF-8"));

                NdefRecord record = NdefRecord.createMime("text/plain",payload);
                records[i] = record;
            }
        }
        records[messagesToSendArray.size()] =
                NdefRecord.createApplicationRecord(getActivity().getPackageName());
        return records;
    }



    public ArrayList<String> handleNfcIntent(Intent NfcIntent) {
        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(NfcIntent.getAction())) {
            Parcelable[] receivedArray =
                    NfcIntent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
            Log.i( "checkbox", "I discovered nfc: ");

            if(receivedArray != null) {
                messagesReceivedArray.clear();
                NdefMessage receivedMessage = (NdefMessage) receivedArray[0];
                NdefRecord[] attachedRecords = receivedMessage.getRecords();
                Log.i( "checkbox", "The received array is not null: ");
                for (NdefRecord record:attachedRecords) {
                    String string = new String(record.getPayload());
                    Log.i("checkbox", "I print the string   " + string);

                    //Make sure we don't pass along our AAR (Android Application Record)
                    if (string.equals("fr.eurecom.introduceme")) {
                        continue;
                    }
                    messagesReceivedArray.add(string);
                    Log.i("checkbox", "Messages " + messagesReceivedArray);

                }
            }
            else {
                Toast.makeText(getActivity(), "Received Blank Parcel", Toast.LENGTH_LONG).show();
            }
        }
        return messagesReceivedArray;
    }




    @Override
    public void onResume() {
        super.onResume();
        handleNfcIntent(getActivity().getIntent());

        messagesToSendArray.clear();
        fillArrayToTransfer();
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainActivity = new MainActivity();
        // Initialize preferences
        preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
       // Toast.makeText(getActivity(), getContext().getPackageName(), Toast.LENGTH_SHORT).show();  // 1st toast  = package name
        messagesToSendArray.clear();
        fillArrayToTransfer();
    }
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.fragmentsend, container, false);
            NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter( getActivity() );
            if (nfcAdapter!=null && nfcAdapter.isEnabled()) {
                //Toast.makeText( getActivity(), "NFC available", Toast.LENGTH_LONG ).show();
            }else {
                Toast.makeText( getActivity(), "NFC not available", Toast.LENGTH_LONG ).show();

                new AlertDialog.Builder(getActivity())
                        .setTitle("IntroduceMe works with NFC")
                        .setMessage("Redirect to phone settings?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                startActivity(new Intent(android.provider.Settings.ACTION_NFC_SETTINGS));
                            }
                        }).setNegativeButton("No", null).show();
            }



            //Check if NFC is available on device
            mNfcAdapter = NfcAdapter.getDefaultAdapter(getActivity());
            if(mNfcAdapter != null) {
                //This will refer back to createNdefMessage for what it will send
                mNfcAdapter.setNdefPushMessageCallback(this, getActivity());

                //This will be called if the message is sent successfully
                mNfcAdapter.setOnNdefPushCompleteCallback(this, getActivity());
            }
            handleNfcIntent(getActivity().getIntent());
            initUI(view);
            return view;
    }

    private void initUI(View view) {
        Button loginlinkedin = view.findViewById( R.id.button_login_linkedin );

        loginlinkedin.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), Linkedin.class); //Linkedin.class);
                startActivity(intent);
            }
        } );
    }

    private void fillArrayToTransfer() {
        boolean checkboxstatefName = preferences.getBoolean( "checkboxstatefName", false );
        String fname = preferences.getString("fname",null);
        if (fname != null){
            messagesToSendArray.add(fname); } else {messagesToSendArray.add("nullValue");}
        boolean checkboxstateName = preferences.getBoolean( "checkboxstateName", false );
        String name = preferences.getString("name",null);
        if (name != null){
            messagesToSendArray.add(name); } else {messagesToSendArray.add("nullValue");}
        boolean checkboxstateFacebook =  preferences.getBoolean( "checkboxstateFacebook", false );
        if (checkboxstateFacebook == true) {
            String facebook = preferences.getString( "facebook", null );
            if (facebook != null) {
                messagesToSendArray.add( facebook ); } else {messagesToSendArray.add("nullValue");}
        } else {messagesToSendArray.add("nullValue");}
        boolean checkboxstateTwitter =  preferences.getBoolean( "checkboxstateTwitter", false );
        if (checkboxstateTwitter == true) {
            String twitter = preferences.getString( "twitter", null );
            if (twitter != null) {
                messagesToSendArray.add( twitter ); } else {messagesToSendArray.add("nullValue");}
        } else {messagesToSendArray.add("nullValue");}
        boolean checkboxstateInstagram =  preferences.getBoolean( "checkboxstateInstagram", false );
        if (checkboxstateInstagram == true) {
            String instagram = preferences.getString( "instagram", null );
            if (instagram != null) {
                messagesToSendArray.add( instagram );} else {messagesToSendArray.add("nullValue");}
        } else {messagesToSendArray.add("nullValue");}
        boolean checkboxstateSnapchat =  preferences.getBoolean( "checkboxstateSnapchat", false );
        if (checkboxstateSnapchat == true) {
            String snapchat = preferences.getString( "snapchat", null );
            if (snapchat != null) {
                messagesToSendArray.add( snapchat );} else {messagesToSendArray.add("nullValue");}
        } else {messagesToSendArray.add("nullValue");}
        boolean checkboxstateLinkedin =  preferences.getBoolean( "checkboxstateLinkedin", false );
        if (checkboxstateLinkedin == true) {
            String linkedin = preferences.getString( "linkedin", null );
            if (linkedin != null) {
                messagesToSendArray.add( linkedin );} else {messagesToSendArray.add("nullValue");}
        } else {messagesToSendArray.add("nullValue");}
        boolean checkboxstateMail =  preferences.getBoolean( "checkboxstateMail", false );
        if (checkboxstateMail == true) {
            String mail = preferences.getString( "mail", null );
            if (mail != null) {
                messagesToSendArray.add( mail );} else {messagesToSendArray.add("nullValue");}
        } else {messagesToSendArray.add("nullValue");}
        boolean checkboxstatePhone =  preferences.getBoolean( "checkboxstatePhone", false );
        if (checkboxstatePhone== true) {
            String phone = preferences.getString( "phone", null );
            if (phone != null) {
                messagesToSendArray.add( phone );} else {messagesToSendArray.add("nullValue");}
        } else {messagesToSendArray.add("nullValue");}
        boolean checkboxstateAddress =  preferences.getBoolean( "checkboxstateAddress", false );
        if (checkboxstateAddress == true) {
            String address = preferences.getString( "address", null );
            if (address != null) {
                messagesToSendArray.add( address );} else {messagesToSendArray.add("nullValue");}
        } else {messagesToSendArray.add("nullValue");}
        boolean checkboxstateCompany =  preferences.getBoolean( "checkboxstateCompany", false );
        if (checkboxstateCompany == true) {
            String company = preferences.getString( "company", null );
            if (company != null) {
                messagesToSendArray.add( company );} else {messagesToSendArray.add("nullValue");}
        } else {messagesToSendArray.add("nullValue");}
        boolean checkboxstateJob =  preferences.getBoolean( "checkboxstateJob", false );
        if (checkboxstateJob == true) {
            String job = preferences.getString( "job", null );
            if (job != null) {
                messagesToSendArray.add( job );} else {messagesToSendArray.add("nullValue");}
        } else {messagesToSendArray.add("nullValue");}
        String picture = preferences.getString( "picture", null );
        if(picture != null){
            messagesToSendArray.add( picture );} else {messagesToSendArray.add( "nullValue" );}
    }
}


package fr.eurecom.introduceme;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;
import java.util.List;

public class ContactRepository {
    ContactDao mContactDao;
    private LiveData<List<Contact>> mAllContacts;
    private LiveData<Contact> me;

    ContactRepository(Application application) {
        AppDatabase db = AppDatabase.getDatabase(application);
        mContactDao = db.contactDao();
        mAllContacts = mContactDao.getAll();
        me = mContactDao.getMe();
    }

    LiveData<List<Contact>> getAll() {
        return mAllContacts;
    }

    LiveData<Contact> getMe() { return me; }

    public void insert (Contact contact) {
        new insertAsyncTask(mContactDao).execute(contact);
    }

    public void updateContacts(Contact... contacts) { new updateAsyncTask(mContactDao).execute(contacts); }

    private static class insertAsyncTask extends AsyncTask<Contact, Void, Void> {

        private ContactDao mAsyncTaskDao;

        insertAsyncTask(ContactDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Contact... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }

    private static class updateAsyncTask extends AsyncTask<Contact, Void, Void> {

        private ContactDao mAsyncTaskDao;

        updateAsyncTask(ContactDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Contact... params) {
            mAsyncTaskDao.updateContacts(params[0]);
            return null;
        }
    }
}

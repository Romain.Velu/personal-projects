package fr.eurecom.introduceme;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.util.Log;

import java.util.List;

public class ContactViewModel extends AndroidViewModel {
    ContactRepository mRepository;
    private LiveData<List<Contact>> mAllContacts;
    private LiveData<Contact> me;

    public ContactViewModel (Application application) {
        super(application);
        mRepository = new ContactRepository(application);
        mAllContacts = mRepository.getAll();
        me = mRepository.getMe();
    }

    LiveData<List<Contact>> getAll() { return mAllContacts; }

    LiveData<Contact> getMe() { return me; }

    public void insert(Contact contact) {
        mRepository.insert(contact); }

    public void updateContacts(Contact... contacts) {mRepository.updateContacts(contacts);}

}

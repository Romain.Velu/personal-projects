package fr.eurecom.introduceme;

import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.util.Log;

public class Preferences extends PreferenceActivity {
    /**
     * Called when the activity is first created.
     */
    public void onCreate(Bundle savedInstanceState) { super.onCreate(savedInstanceState);
//load the preferences from an xml rsource
        addPreferencesFromResource(R.xml.preferences);
        Log.i("Main", "the Preferences class just called!");

    }
}
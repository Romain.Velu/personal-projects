package fr.eurecom.introduceme;

import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

public class MySavedCard extends AppCompatActivity {
    android.content.SharedPreferences preferences;
    private Contact mContact;
    SwitchCompat checkboxFacebook,checkboxTwitter,checkboxInstagram,checkboxSnapchat,checkboxLinkedin,checkboxMail,checkboxPhone,checkboxAddress,checkboxCompany,checkboxJob;
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mysavedcar);

        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        readSavedPreferences();

        makeCheckboxes();
    }

    private void makeCheckboxes(){
        checkboxFacebook = (SwitchCompat) findViewById( R.id.checkboxFacebook ) ;
        checkboxFacebook.setChecked(preferences.getBoolean( "checkboxstateFacebook",false ));
        checkboxFacebook.setOnClickListener( new OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checkboxstate = checkboxFacebook.isChecked();
                SharedPreferences.Editor edit = preferences.edit();
                edit.putBoolean("checkboxstateFacebook", checkboxstate);
                edit.commit();
                Log.v("checkbox", "boolean value o checkbox: " + checkboxstate);
            }
        } );
        checkboxTwitter = (SwitchCompat) findViewById( R.id.checkboxTwitter ) ;
        checkboxTwitter.setChecked(preferences.getBoolean( "checkboxstateTwitter",false ));
        checkboxTwitter.setOnClickListener( new OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checkboxstate = checkboxTwitter.isChecked();
                SharedPreferences.Editor edit = preferences.edit();
                edit.putBoolean("checkboxstateTwitter", checkboxstate);
                edit.commit();
                Log.v("checkbox", "boolean value o checkbox: " + checkboxstate);
            }
        } );
        checkboxInstagram = (SwitchCompat) findViewById( R.id.checkboxInstagram ) ;
        checkboxInstagram.setChecked(preferences.getBoolean( "checkboxstateInstagram",false ));
        checkboxInstagram.setOnClickListener( new OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checkboxstate = checkboxInstagram.isChecked();
                SharedPreferences.Editor edit = preferences.edit();
                edit.putBoolean("checkboxstateInstagram", checkboxstate);
                edit.commit();
                Log.v("checkbox", "boolean value o checkbox: " + checkboxstate);
            }
        } );
        checkboxSnapchat = (SwitchCompat) findViewById( R.id.checkboxSnapchat ) ;
        checkboxSnapchat.setChecked(preferences.getBoolean( "checkboxstateSnapchat",false ));
        checkboxSnapchat.setOnClickListener( new OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checkboxstate = checkboxSnapchat.isChecked();
                SharedPreferences.Editor edit = preferences.edit();
                edit.putBoolean("checkboxstateSnapchat", checkboxstate);
                edit.commit();
                Log.v("checkbox", "boolean value o checkbox: " + checkboxstate);
            }
        } );
        checkboxLinkedin = (SwitchCompat) findViewById( R.id.checkboxLinkedin ) ;
        checkboxLinkedin.setChecked(preferences.getBoolean( "checkboxstateLinkedin",false ));
        checkboxLinkedin.setOnClickListener( new OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checkboxstate = checkboxLinkedin.isChecked();
                SharedPreferences.Editor edit = preferences.edit();
                edit.putBoolean("checkboxstateLinkedin", checkboxstate);
                edit.commit();
                Log.v("checkbox", "boolean value o checkbox: " + checkboxstate);
            }
        } );
        checkboxMail = (SwitchCompat) findViewById( R.id.checkboxMail ) ;
        checkboxMail.setChecked(preferences.getBoolean( "checkboxstateMail",false ));
        checkboxMail.setOnClickListener( new OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checkboxstate = checkboxMail.isChecked();
                SharedPreferences.Editor edit = preferences.edit();
                edit.putBoolean("checkboxstateMail", checkboxstate);
                edit.commit();
                Log.v("checkbox", "boolean value o checkbox: " + checkboxstate);
            }
        } );
        checkboxPhone = (SwitchCompat) findViewById( R.id.checkboxPhone ) ;
        checkboxPhone.setChecked(preferences.getBoolean( "checkboxstatePhone",false ));
        checkboxPhone.setOnClickListener( new OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checkboxstate = checkboxPhone.isChecked();
                SharedPreferences.Editor edit = preferences.edit();
                edit.putBoolean("checkboxstatePhone", checkboxstate);
                edit.commit();
                Log.v("checkbox", "boolean value o checkbox: " + checkboxstate);
            }
        } );
        checkboxAddress = (SwitchCompat) findViewById( R.id.checkboxAddress ) ;
        checkboxAddress.setChecked(preferences.getBoolean( "checkboxstateAddress",false ));
        checkboxAddress.setOnClickListener( new OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checkboxstate = checkboxAddress.isChecked();
                SharedPreferences.Editor edit = preferences.edit();
                edit.putBoolean("checkboxstateAddress", checkboxstate);
                edit.commit();
                Log.v("checkbox", "boolean value o checkbox: " + checkboxstate);
            }
        } );
        checkboxCompany = (SwitchCompat) findViewById( R.id.checkboxCompany ) ;
        checkboxCompany.setChecked(preferences.getBoolean( "checkboxstateCompany",false ));
        checkboxCompany.setOnClickListener( new OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checkboxstate = checkboxCompany.isChecked();
                SharedPreferences.Editor edit = preferences.edit();
                edit.putBoolean("checkboxstateCompany", checkboxstate);
                edit.commit();
                Log.v("checkbox", "boolean value o checkbox: " + checkboxstate);
            }
        } );
        checkboxJob = (SwitchCompat) findViewById( R.id.checkboxJob ) ;
        checkboxJob.setChecked(preferences.getBoolean( "checkboxstateJob",false ));
        checkboxJob.setOnClickListener( new OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checkboxstate = checkboxJob.isChecked();
                SharedPreferences.Editor edit = preferences.edit();
                edit.putBoolean("checkboxstateJob", checkboxstate);
                edit.commit();
                Log.v("checkbox", "boolean value o checkbox: " + checkboxstate);
            }
        } );

    }

    private void readSavedPreferences(){
        TextView saved_fname = findViewById(R.id.textViewfName);
        saved_fname.setText(preferences.getString("fname",null));
        TextView saved_name = findViewById(R.id.textViewName);
        saved_name.setText(preferences.getString("name",null));
        TextView new_facebook = findViewById(R.id.textViewFacebook);
        new_facebook.setText(preferences.getString("facebook",null ));
        TextView new_twitter = findViewById(R.id.textViewTwitter);
        new_twitter.setText(preferences.getString("twitter",null));
        TextView new_instagram = findViewById(R.id.textViewInstagram);
        new_instagram.setText(preferences.getString("instagram",null));
        TextView new_snapchat = findViewById(R.id.textViewSnapchat);
        new_snapchat.setText(preferences.getString("snapchat",null));
        TextView new_linkedin = findViewById(R.id.textViewLinkedin);
        new_linkedin.setText(preferences.getString("linkedin",null));
        TextView new_mail = findViewById(R.id.textViewMail);
        new_mail.setText(preferences.getString("mail",null));
        TextView new_phone = findViewById(R.id.textViewPhone);
        new_phone.setText(preferences.getString("phone",null));
        TextView new_address = findViewById(R.id.textViewAddress);
        new_address.setText(preferences.getString("address",null));
        TextView new_company = findViewById(R.id.textViewCompany);
        new_company.setText(preferences.getString("company",null));
        TextView new_job = findViewById(R.id.textViewJob);
        new_job.setText(preferences.getString("job",null));
        if(preferences.getString( "picture", null ) != null){
            ImageView new_pic = findViewById( R.id.pictureview2 );
            Uri myuri = Uri.parse(Uri.decode(preferences.getString( "picture",null )));
            //new_pic.setImageURI(myuri);
            Log.d( "path", preferences.getString( "picture", null ) );
        }
    }
}

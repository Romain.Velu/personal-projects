package fr.eurecom.introduceme;

import android.Manifest;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;




public class PopupWindow extends AppCompatActivity {
    public ArrayList<String> messagesPopArray = new ArrayList<>();
    public MainActivity mainActivity;
    public TextView textPopupFname, textPopupName, textPopupPhone, textPopupMail, textPopupAddress, textPopupFacebook, textPopupTwitter, textPopupInstagram, textPopupSnapchat, textPopupLinkedin, textPopupCompany, textPopupJob;
    private ContactViewModel mContactViewModel;
    private Contact contactreceived;
    public LocationManager locationMangaer=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

            setTitle("Received card");

        mainActivity = new MainActivity();
        mContactViewModel = ViewModelProviders.of(this).get(ContactViewModel.class);

        setContentView(R.layout.popup_window_layout);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int) (width * .8), (int) (height * .75));

        locationMangaer   = (LocationManager)getSystemService(Context.LOCATION_SERVICE);

        mContactViewModel = ViewModelProviders.of(this).get(ContactViewModel.class);
        List<List<String>> names = new ArrayList<>();
        mContactViewModel.getAll().observe(this, new Observer<List<Contact>>() {
            @Override
            public void onChanged(@Nullable final List<Contact> words) {
                // Update the cached copy of the words in the adapter.
                //adapter.setContact(words);
                for(int i=0; i<words.size(); i++){
                    words.get( i ).getFirstName();
                    if(words.get( i ).getFirstName() != null){
                        List<String> names_ = new ArrayList<>();
                        names_.add(words.get(i).getFirstName());
                        names_.add(words.get( i ).getLastName() );
                        names.add(names_);
                        Log.i("names", " "+names_);
                    }
                }
            }
        });

        FloatingActionButton myFab = (FloatingActionButton) findViewById(R.id.myFab);
        myFab.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Date c = Calendar.getInstance().getTime();
                SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                String formattedDate = df.format(c);
                contactreceived.setMetDate(formattedDate);
                boolean equals = false;
                contactreceived.setMetLocation(mainActivity.cityname);
                if(names !=null) {
                    for (int i = 0; i < names.size(); i++) {
                        if (contactreceived.getFirstName().equalsIgnoreCase(names.get(i).get(0)) && contactreceived.getLastName().equalsIgnoreCase(names.get(i).get(1))) {
                            equals = true;
                        }
                    }
                }
                if(equals == false) {
                    mContactViewModel.insert( contactreceived );
                    Toast.makeText(getBaseContext(),"Contact added!",
                            Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(getBaseContext(),"Already saved!",
                            Toast.LENGTH_SHORT).show();
                }
                finish();
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();


        Intent i = getIntent();
        messagesPopArray = i.getStringArrayListExtra("messages");
        contactreceived = new Contact();

        String s_received_fname = messagesPopArray.get(0);
        TextView received_fname = (TextView)findViewById(R.id.textPopupFname);
        if (!s_received_fname.equals( "nullValue" )) {
            received_fname.setText( s_received_fname);
            contactreceived.setFirstName(s_received_fname); }

        String s_received_name = messagesPopArray.get(1);
        TextView received_name = (TextView)findViewById(R.id.textPopupName);
        if (!s_received_name.equals("nullValue")) {
            received_name.setText(s_received_name);
            contactreceived.setLastName(s_received_name); }

        String s_received_facebook= messagesPopArray.get(2);
        TextView received_facebook = (TextView)findViewById(R.id.textPopupFacebook);
        if (!s_received_facebook.equals("nullValue")) {
            received_facebook.setText(s_received_facebook);
            contactreceived.setFacebookAcc(s_received_facebook); }

        String s_received_twitter = messagesPopArray.get(3);
        TextView received_twitter = (TextView)findViewById(R.id.textPopupTwitter);
        if (!s_received_twitter.equals("nullValue")) {
            received_twitter.setText(s_received_twitter);
            contactreceived.setTwitterAcc(s_received_twitter); }

        String s_received_instagram = messagesPopArray.get(4);
        TextView received_instagram = (TextView)findViewById(R.id.textPopupInstagram);
        if (!s_received_instagram.equals("nullValue")) {
            received_instagram.setText(s_received_instagram);
            contactreceived.setInstaAcc(s_received_instagram); }

        String s_received_snapchat= messagesPopArray.get(5);
        TextView received_snapchat = (TextView)findViewById(R.id.textPopupSnapchat);
        if (!s_received_snapchat.equals("nullValue")) {
            received_snapchat.setText(s_received_snapchat);
            contactreceived.setSnapchatAccount(s_received_snapchat); }


        String s_received_linkedin = messagesPopArray.get(6);
        TextView received_linkedin= (TextView)findViewById(R.id.textPopupLinkedin);
        if (!s_received_linkedin.equals("nullValue")) {
            received_linkedin.setText(s_received_linkedin);
            contactreceived.setLinkedInAcc(s_received_linkedin); }


        String s_received_mail = messagesPopArray.get(7);
        TextView received_mail = (TextView)findViewById(R.id.textPopupMail);
        if (!s_received_mail.equals("nullValue")) {
            received_mail.setText(s_received_mail);
            contactreceived.setMailAddress(s_received_mail); }

        String s_received_phone = messagesPopArray.get(8);
        TextView received_phone = (TextView)findViewById(R.id.textPopupPhone);

        if (!s_received_phone.equals("nullValue") && !s_received_phone.equals("")) {
            received_phone.setText(s_received_phone);
            contactreceived.setMobileNumber(Integer.parseInt(s_received_phone)); }

        String s_received_address = messagesPopArray.get(9);
        TextView received_address = (TextView)findViewById(R.id.textPopupAddress);
        if (!s_received_address.equals("nullValue")) {
            received_address.setText(s_received_address);
            contactreceived.setLivingAddress(s_received_address); }


        String s_received_company = messagesPopArray.get(10);
        TextView received_company = (TextView)findViewById(R.id.textPopupCompany);
        if (!s_received_company.equals("nullValue")) {
            received_company.setText(s_received_company);
            contactreceived.setCompany(s_received_company); }

        String s_received_job = messagesPopArray.get(11);
        TextView received_job = (TextView)findViewById(R.id.textPopupJob);
        if (!s_received_job.equals("nullValue")) {
            received_job.setText(s_received_job);
            contactreceived.setJob(s_received_job); }

        String s_received_picture = messagesPopArray.get(11);
        ImageView picture = (ImageView) findViewById(R.id.picture);
        if (!s_received_picture.equals("nullValue")) {
        }
    }
}

package fr.eurecom.introduceme;


import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import io.reactivex.annotations.NonNull;

@Entity(tableName = "contacts",
        indices = {@Index(value = {"first_name", "last_name"}, unique = true)})

public class Contact {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int id;

    @ColumnInfo(name = "is_me")
    private int isMe = 0;

    @NonNull
    @ColumnInfo(name = "first_name")
    private String firstName;

    @NonNull
    @ColumnInfo(name = "last_name")
    private String lastName;

    @ColumnInfo(name = "mobile_number")
    private int mobileNumber;

    @ColumnInfo(name = "mail_address")
    private String mailAddress;

    @ColumnInfo(name = "living_address")
    private String livingAddress;

    @ColumnInfo(name = "facebook_account")
    private String FacebookAcc;

    @ColumnInfo(name = "twitter_account")
    private String TwitterAcc;

    @ColumnInfo(name = "intsagram_account")
    private String InstaAcc;

    @ColumnInfo(name = "snapchat_account")
    private String SnapchatAccount;

    @ColumnInfo(name = "linkedIn_account")
    private String LinkedInAcc;

    @ColumnInfo(name = "company")
    private String company;

    @ColumnInfo(name = "job")
    private String job;

    @ColumnInfo(name = "metLocation")
    private String metLocation;

    @ColumnInfo(name = "metDate")
    private String metDate;

    @ColumnInfo(name = "picture")
    private String myPic;



    public Contact() {}

    public Integer getId() {
        return id;
    }

    public void setId(Integer Sid) {
        this.id = Sid;
    }

    public int getIsMe() { return isMe; }

    public void setIsMe(int SisMe) {
        this.isMe = SisMe;
    }

    public String getFirstName() { return firstName; }

    public void setFirstName(String SfirstName) {
        this.firstName = SfirstName;
    }

    public String getLastName() { return lastName; }

    public void setLastName(String SlastName) {
        this.lastName = SlastName;
    }

    public int getMobileNumber() { return mobileNumber; }

    public void setMobileNumber(int SmobileNumber) {
        this.mobileNumber = SmobileNumber;
    }

    public String getMailAddress() { return mailAddress; }

    public void setMailAddress(String SmailAddress) {
        this.mailAddress = SmailAddress;
    }

    public String getLivingAddress() { return livingAddress; }

    public void setLivingAddress(String SlivingAddress) {
        this.livingAddress = SlivingAddress;
    }

    public String getFacebookAcc() { return FacebookAcc; }

    public void setFacebookAcc(String SFacebookAcc) {
        this.FacebookAcc = SFacebookAcc;
    }

    public String getTwitterAcc() { return TwitterAcc; }

    public void setTwitterAcc(String STwitterAcc) {
        this.TwitterAcc = STwitterAcc;
    }

    public String getInstaAcc() { return InstaAcc; }

    public void setInstaAcc(String SInstaAcc) {
        this.InstaAcc = SInstaAcc;
    }

    public String getSnapchatAccount() { return SnapchatAccount; }

    public void setSnapchatAccount(String SSnapchatAccount) {
        this.SnapchatAccount = SSnapchatAccount;
    }

    public String getLinkedInAcc() { return LinkedInAcc; }

    public void setLinkedInAcc(String SLinkedInAcc) {
        this.LinkedInAcc = SLinkedInAcc;
    }

    public String getCompany() { return company; }

    public void setCompany(String Scompany) {
        this.company = Scompany;
    }

    public String getJob() { return job; }

    public void setJob(String Sjob) {
        this.job = Sjob;
    }

    public String getMetLocation() { return metLocation; }

    public void setMetLocation(String SmetLocation) { this.metLocation = SmetLocation; }

    public String getMetDate() { return metDate; }

    public void setMetDate(String SmetDate) { this.metDate = SmetDate; }

    public String getMyPic(){ return myPic; }

    public void setMyPic(String SmyPic) { this.myPic = SmyPic; }
}

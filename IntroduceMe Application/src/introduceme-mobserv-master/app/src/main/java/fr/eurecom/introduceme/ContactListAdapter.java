package fr.eurecom.introduceme;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class ContactListAdapter extends RecyclerView.Adapter<ContactListAdapter.ContactViewHolder> {
        public static Integer IDcurrentContact = 0;


        class ContactViewHolder extends RecyclerView.ViewHolder {
            private final TextView contactItemView;

            private ContactViewHolder(View itemView) {
                super(itemView);
                contactItemView = itemView.findViewById(R.id.textView);
            }
        }


        private final LayoutInflater mInflater;
        private List<Contact> mContacts; // Cached copy of contacts

        ContactListAdapter(Context context) { mInflater = LayoutInflater.from(context); }

        @Override
        public ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = mInflater.inflate(R.layout.recyclerview_item, parent, false);
            return new ContactViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(ContactViewHolder holder, int position) {
            if (mContacts != null) {
                final Contact current = mContacts.get(position); //final Contact current = mContacts.get(position);
                IDcurrentContact = current.getId();
                holder.contactItemView.setText(current.getFirstName()+" "+current.getLastName());

                holder.contactItemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Context context = holder.contactItemView.getContext();
                        Intent intent = new Intent(context, HisOrHerInfo.class);
                        intent.putExtra("the contact ID", current.getId());
                        context.startActivity(intent);
                    }
                });

            } else {
                // Covers the case of data not being ready yet.
                holder.contactItemView.setText("No Contact");
            }
        }

        void setContact(List<Contact> contacts){
            mContacts = contacts;
            notifyDataSetChanged();
        }

        // getItemCount() is called many times, and when it is first called,
        // mContacts has not been updated (means initially, it's null, and we can't return null).
        @Override
        public int getItemCount() {
            if (mContacts != null)
                 return mContacts.size();//return mContacts.size();
            else return 0;
        }


}


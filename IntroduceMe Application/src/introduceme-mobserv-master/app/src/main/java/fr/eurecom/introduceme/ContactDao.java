package fr.eurecom.introduceme;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface ContactDao {

    @Insert
    void insert(Contact contact);

    @Query("DELETE FROM contacts")
    void deleteAll();

    @Update
    void updateContacts(Contact... contacts);

    // all those methods are from https://developer.android.com/training/data-storage/room/
    // we'll see later what we need exactly

    @Query("SELECT * FROM contacts")
    LiveData<List<Contact>> getAll();

    @Query("SELECT * FROM contacts")
    List<Contact> getAll2();

    @Query("SELECT * FROM contacts WHERE is_me=1")
    LiveData<Contact> getMe();

    @Query("SELECT * FROM contacts WHERE id = (:userId)")
    LiveData<Contact> loadById(int userId);

    @Query("SELECT * FROM contacts WHERE first_name LIKE :first AND "
            + "last_name LIKE :last LIMIT 1")
    Contact findByName(String first, String last);

    @Insert
    void insertAll(Contact... contacts);

    @Delete
    void delete(Contact contact);
}


package fr.eurecom.introduceme;

import android.Manifest;
import android.app.Dialog;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.media.Image;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.InputType;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.View.OnClickListener;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.NoSuchElementException;

import static android.app.Activity.RESULT_OK;

public class MyInfo extends AppCompatActivity {
    android.content.SharedPreferences preferences;

    private int PICK_IMAGE_REQUEST = 1;
    CheckBox checkboxfName,checkboxName,checkboxFacebook,checkboxTwitter,checkboxInstagram,checkboxSnapchat,checkboxLinkedin,checkboxMail,checkboxPhone,checkboxAddress,checkboxCompany,checkboxJob;
    final Context context = this;
    private ContactViewModel mContactViewModel;
    private Contact mContact;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.myinfo_layout);  //instatiating the myinfo layout

// Reading the saved preferences values.
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        readSavedPreferences();

//Instatiating the buttons and their actions
        makeButtons();


        mContactViewModel = ViewModelProviders.of(this).get(ContactViewModel.class);
        mContact = new Contact();

        mContactViewModel.getMe().observe( (LifecycleOwner) this, new Observer<Contact>() {
            @Override
            public void onChanged(@Nullable final Contact metry) {
                if(metry == null ) {
                    //Toast.makeText(getApplicationContext(),"wait...",Toast.LENGTH_LONG).show();
                }else{
                    mContact.setId(metry.getId());
                }
            }
        });
        TextView new_fname = findViewById(R.id.textViewfName);
        String previous_fname = new_fname.getText().toString();
        mContact.setFirstName(previous_fname);
        TextView new_name = findViewById(R.id.textViewName);
        String previous_name = new_name.getText().toString();
        mContact.setLastName(previous_name);
        TextView new_facebook = findViewById(R.id.textViewFacebook);
        String previous_facebook = new_facebook.getText().toString();
        mContact.setFacebookAcc(previous_facebook);
        TextView new_twitter = findViewById(R.id.textViewTwitter);
        String previous_twitter = new_twitter.getText().toString();
        mContact.setTwitterAcc(previous_twitter);
        TextView new_instagram = findViewById(R.id.textViewInstagram);
        String previous_instagram = new_instagram.getText().toString();
        mContact.setInstaAcc(previous_instagram);
        TextView new_snapchat = findViewById(R.id.textViewSnapchat);
        String previous_snapchat = new_snapchat.getText().toString();
        mContact.setSnapchatAccount(previous_snapchat);
        TextView new_linkedin = findViewById(R.id.textViewLinkedin);
        String previous_linkedin = new_linkedin.getText().toString();
        mContact.setLinkedInAcc(previous_linkedin);
        TextView new_mail = findViewById(R.id.textViewMail);
        String previous_mail = new_mail.getText().toString();
        mContact.setMailAddress(previous_mail);
        TextView new_phone = findViewById(R.id.textViewPhone);
        String previous_phone = new_phone.getText().toString();
        if (previous_phone != "") {
            mContact.setMobileNumber(Integer.parseInt(previous_phone));
        }
        TextView new_address = findViewById(R.id.textViewAddress);
        String previous_address = new_address.getText().toString();
        mContact.setLivingAddress(previous_address);
        TextView new_company = findViewById(R.id.textViewCompany);
        String previous_company = new_company.getText().toString();
        mContact.setCompany(previous_company);
        TextView new_job = findViewById(R.id.textViewJob);
        String previous_job = new_job.getText().toString();
        mContact.setJob(previous_job);
        //ImageView new_pic = findViewById( R.id.pictureview );
        //String previous_pic = new_pic.getTag().toString();
        //mContact.setMyPic( previous_pic );


        FloatingActionButton save = findViewById(R.id.floatingActionButton3);
        save.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mContact.setIsMe(1);
                mContactViewModel.updateContacts(mContact);
                Toast.makeText(getApplicationContext(),"Changes saved",Toast.LENGTH_LONG).show();
            }
        });
    }

    private void readSavedPreferences(){
        TextView saved_fname = findViewById(R.id.textViewfName);
        saved_fname.setText(preferences.getString("fname",null));
        TextView saved_name = findViewById(R.id.textViewName);
        saved_name.setText(preferences.getString("name",null));
        TextView new_facebook = findViewById(R.id.textViewFacebook);
        new_facebook.setText(preferences.getString("facebook",null ));
        TextView new_twitter = findViewById(R.id.textViewTwitter);
        new_twitter.setText(preferences.getString("twitter",null));
        TextView new_instagram = findViewById(R.id.textViewInstagram);
        new_instagram.setText(preferences.getString("instagram",null));
        TextView new_snapchat = findViewById(R.id.textViewSnapchat);
        new_snapchat.setText(preferences.getString("snapchat",null));
        TextView new_linkedin = findViewById(R.id.textViewLinkedin);
        new_linkedin.setText(preferences.getString("linkedin",null));
        TextView new_mail = findViewById(R.id.textViewMail);
        new_mail.setText(preferences.getString("mail",null));
        TextView new_phone = findViewById(R.id.textViewPhone);
        new_phone.setText(preferences.getString("phone",null));
        TextView new_address = findViewById(R.id.textViewAddress);
        new_address.setText(preferences.getString("address",null));
        TextView new_company = findViewById(R.id.textViewCompany);
        new_company.setText(preferences.getString("company",null));
        TextView new_job = findViewById(R.id.textViewJob);
        new_job.setText(preferences.getString("job",null));
        if(preferences.getString( "picture", null ) != null){
            ImageView new_pic = findViewById( R.id.pictureview );
            Uri myuri = Uri.parse(Uri.decode(preferences.getString( "picture",null )));
            new_pic.setImageURI(myuri);
            Log.d( "path", preferences.getString( "picture", null ) );
        }
    }


    private void makeButtons(){
        Button fname = findViewById(R.id.button_fname);
        // add button listener
        fname.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView new_fname = findViewById(R.id.textViewfName);
                TextView new_name = findViewById(R.id.textViewName);
                String previous_fname = new_fname.getText().toString();
                String previous_name = new_name.getText().toString();
                mContact.setFirstName(previous_fname);
                mContact.setLastName(previous_name);
                // custom dialog
                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.custom);
                dialog.setTitle("Validate");
                final EditText text = dialog.findViewById(R.id.editText);
                text.setText(previous_fname +" "+ previous_name);
                text.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS);
                Button okButton = dialog.findViewById(R.id.okButton);
                okButton.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        String name = text.getText().toString();
                        String[] separated = name.split(" ");
                        TextView new_fname = findViewById(R.id.textViewfName);
                        new_fname.setText(separated[0]);
                        TextView new_name = findViewById(R.id.textViewName);
                        new_name.setText(separated[1]);

                        SharedPreferences.Editor edit = preferences.edit();
                        edit.putString("fname", separated[0]);
                        edit.putString("name", separated[1]);
                        edit.commit();

                        mContact.setFirstName(separated[0]);
                        mContact.setLastName(separated[1]);
                    }
                });
                Button cancelButton = dialog.findViewById(R.id.cancelButton);
                cancelButton.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });

        Button facebook = findViewById(R.id.button_facebook);
        // add button listener
        facebook.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView new_facebook = findViewById(R.id.textViewFacebook);
                String previous_facebook = new_facebook.getText().toString();
                // custom dialog
                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.custom);
                dialog.setTitle("Validate");
                final EditText text = dialog.findViewById(R.id.editText);
                text.setText(previous_facebook);
                Button okButton = dialog.findViewById(R.id.okButton);
                okButton.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        String facebook = text.getText().toString();
                        TextView new_facebook = findViewById(R.id.textViewFacebook);
                        new_facebook.setText(facebook);

                        SharedPreferences.Editor edit = preferences.edit();
                        edit.putString("facebook", facebook);
                        edit.commit();
                        mContact.setFacebookAcc(facebook);
                    }
                });
                Button cancelButton = dialog.findViewById(R.id.cancelButton);
                cancelButton.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });


        Button twitter = findViewById(R.id.button_twitter);
        // add button listener
        twitter.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView new_twitter = findViewById(R.id.textViewTwitter);
                String previous_twitter = new_twitter.getText().toString();
                // custom dialog
                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.custom);
                dialog.setTitle("Validate");
                final EditText text = dialog.findViewById(R.id.editText);
                text.setText(previous_twitter);
                Button okButton = dialog.findViewById(R.id.okButton);
                okButton.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        String twitter = text.getText().toString();
                        TextView new_twitter = findViewById(R.id.textViewTwitter);
                        new_twitter.setText(twitter);

                        SharedPreferences.Editor edit = preferences.edit();
                        edit.putString("twitter", twitter);
                        edit.commit();
                        mContact.setTwitterAcc(twitter);
                    }
                });
                Button cancelButton = dialog.findViewById(R.id.cancelButton);
                cancelButton.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });


        Button instagram = findViewById(R.id.button_instagram);
        // add button listener
        instagram.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView new_instagram = findViewById(R.id.textViewInstagram);
                String previous_instagram = new_instagram.getText().toString();
                // custom dialog
                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.custom);
                dialog.setTitle("Validate");
                final EditText text = dialog.findViewById(R.id.editText);
                text.setText(previous_instagram);
                Button okButton = dialog.findViewById(R.id.okButton);
                okButton.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        String instagram = text.getText().toString();
                        TextView new_instagram = findViewById(R.id.textViewInstagram);
                        new_instagram.setText(instagram);

                        SharedPreferences.Editor edit = preferences.edit();
                        edit.putString("instagram", instagram);
                        edit.commit();
                        mContact.setInstaAcc(instagram);
                    }
                });
                Button cancelButton = dialog.findViewById(R.id.cancelButton);
                cancelButton.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });


        Button snapchat = findViewById(R.id.button_snapchat);
        // add button listener
        snapchat.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView new_snapchat = findViewById(R.id.textViewSnapchat);
                String previous_snapchat = new_snapchat.getText().toString();
                // custom dialog
                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.custom);
                dialog.setTitle("Validate");
                final EditText text = dialog.findViewById(R.id.editText);
                text.setText(previous_snapchat);
                Button okButton = dialog.findViewById(R.id.okButton);
                okButton.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        String snapchat = text.getText().toString();
                        TextView new_snapchat = findViewById(R.id.textViewSnapchat);
                        new_snapchat.setText(snapchat);

                        SharedPreferences.Editor edit = preferences.edit();
                        edit.putString("snapchat", snapchat);
                        edit.commit();
                        mContact.setSnapchatAccount(snapchat);
                    }
                });
                Button cancelButton = dialog.findViewById(R.id.cancelButton);
                cancelButton.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });


        Button linkedin = findViewById(R.id.button_linkedin);
        // add button listener
        linkedin.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView new_linkedin = findViewById(R.id.textViewLinkedin);
                String previous_linkedin = new_linkedin.getText().toString();
                // custom dialog
                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.custom);
                dialog.setTitle("Validate");
                final EditText text = dialog.findViewById(R.id.editText);
                text.setText(previous_linkedin);
                Button okButton = dialog.findViewById(R.id.okButton);
                okButton.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        String linkedin = text.getText().toString();
                        TextView new_linkedin = findViewById(R.id.textViewLinkedin);
                        new_linkedin.setText(linkedin);

                        SharedPreferences.Editor edit = preferences.edit();
                        edit.putString("linkedin", linkedin);
                        edit.commit();
                        mContact.setLinkedInAcc(linkedin);
                    }
                });
                Button cancelButton = dialog.findViewById(R.id.cancelButton);
                cancelButton.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });


        Button mail = findViewById(R.id.button_mail);
        // add button listener
        mail.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView new_mail = findViewById(R.id.textViewMail);
                String previous_mail = new_mail.getText().toString();
                // custom dialog
                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.custom);
                dialog.setTitle("Validate");
                final EditText text = dialog.findViewById(R.id.editText);
                text.setText(previous_mail);
                text.setInputType(InputType.TYPE_TEXT_VARIATION_WEB_EMAIL_ADDRESS);
                Button okButton = dialog.findViewById(R.id.okButton);
                okButton.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        String mail = text.getText().toString();
                        TextView new_mail = findViewById(R.id.textViewMail);
                        new_mail.setText(mail);

                        SharedPreferences.Editor edit = preferences.edit();
                        edit.putString("mail", mail);
                        edit.commit();
                        mContact.setMailAddress(mail);
                    }
                });
                Button cancelButton = dialog.findViewById(R.id.cancelButton);
                cancelButton.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });


        Button phone = findViewById(R.id.button_phone);
        // add button listener
        phone.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView new_phone = findViewById(R.id.textViewPhone);
                String previous_phone = new_phone.getText().toString();
                // custom dialog
                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.custom);
                dialog.setTitle("Validate");
                final EditText text = dialog.findViewById(R.id.editText);
                text.setText(previous_phone);
                text.setInputType(InputType.TYPE_CLASS_PHONE);
                Button okButton = dialog.findViewById(R.id.okButton);
                okButton.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        String phone = text.getText().toString();
                        TextView new_phone = findViewById(R.id.textViewPhone);
                        new_phone.setText(phone);

                        SharedPreferences.Editor edit = preferences.edit();
                        edit.putString("phone", phone);
                        edit.commit();
                        mContact.setMobileNumber(Integer.parseInt(phone));
                    }
                });
                Button cancelButton = dialog.findViewById(R.id.cancelButton);
                cancelButton.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });


        Button address = findViewById(R.id.button_address);
        // add button listener
        address.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView new_address = findViewById(R.id.textViewAddress);
                String previous_address = new_address.getText().toString();
                // custom dialog
                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.custom);
                dialog.setTitle("Validate");
                final EditText text = dialog.findViewById(R.id.editText);
                text.setText(previous_address);
                text.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
                Button okButton = dialog.findViewById(R.id.okButton);
                okButton.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        String address = text.getText().toString();
                        TextView new_address = findViewById(R.id.textViewAddress);
                        new_address.setText(address);

                        SharedPreferences.Editor edit = preferences.edit();
                        edit.putString("address", address);
                        edit.commit();
                        mContact.setLivingAddress(address);
                    }
                });
                Button cancelButton = dialog.findViewById(R.id.cancelButton);
                cancelButton.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });


        Button company = findViewById(R.id.button_company);
        // add button listener
        company.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView new_company = findViewById(R.id.textViewCompany);
                String previous_company = new_company.getText().toString();
                // custom dialog
                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.custom);
                dialog.setTitle("Validate");
                final EditText text = dialog.findViewById(R.id.editText);
                text.setText(previous_company);
                text.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
                Button okButton = dialog.findViewById(R.id.okButton);
                okButton.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        String company = text.getText().toString();
                        TextView new_company = findViewById(R.id.textViewCompany);
                        new_company.setText(company);

                        SharedPreferences.Editor edit = preferences.edit();
                        edit.putString("company", company);
                        edit.commit();
                        mContact.setCompany(company);
                    }
                });
                Button cancelButton = dialog.findViewById(R.id.cancelButton);
                cancelButton.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });


        Button job = findViewById(R.id.button_job);
        // add button listener
        job.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView new_job = findViewById(R.id.textViewJob);
                String previous_job = new_job.getText().toString();
                // custom dialog
                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.custom);
                dialog.setTitle("Validate");
                final EditText text = dialog.findViewById(R.id.editText);
                text.setText(previous_job);
                text.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
                Button okButton = dialog.findViewById(R.id.okButton);
                okButton.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        String job = text.getText().toString();
                        TextView new_job = findViewById(R.id.textViewJob);
                        new_job.setText(job);

                        SharedPreferences.Editor edit = preferences.edit();
                        edit.putString("job", job);
                        edit.commit();
                        mContact.setJob(job);
                    }
                });
                Button cancelButton = dialog.findViewById(R.id.cancelButton);
                cancelButton.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });

        ImageView picture = findViewById(R.id.pictureview);
        picture.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pickPhoto = new Intent(Intent.ACTION_OPEN_DOCUMENT,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto , 1);
            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        if(resultCode == RESULT_OK && requestCode == 1){
            Uri selectedImage = imageReturnedIntent.getData();
            ImageView mypic = findViewById( R.id.pictureview );
            mypic.setImageURI(selectedImage);
            SharedPreferences.Editor edit = preferences.edit();
            edit.putString("picture", selectedImage.toString());
            edit.commit();
            mContact.setMyPic(selectedImage.toString());
        }
    }
}

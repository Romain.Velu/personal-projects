# Project Name

### IntroduceMe
![Logo](IntroduceMe Application/logo/introducemelogo.png)

## Platform

Android

### Description

Create your prefered profile of information and share it with your peers using the NFC technology.

Socialize instantly and keep track of your contacts!

Information to be configured:
```
e-Mail, Phone, Address, Job,  LinkedIn, Facebook, Instagram, etc.
```

## Main Features

- Exchange personal information easily
- Simulation of a business card
- Modify this card at any time
- Store business cards of others

## Used frameworks
- Fragment
- Intent
- Room
- NfcAdapter

## Required permissions
- NFC
- Read and write contacts
- Read external storage
- Internet
- Location

## Screenshots
![Logo](IntroduceMe Application/screenshots/1.png)
![Logo](IntroduceMe Application/screenshots/2.png)
![Logo](IntroduceMe Application/screenshots/3.png)
![Logo](IntroduceMe Application/screenshots/4.png)
![Logo](IntroduceMe Application/screenshots/5.png)
![Logo](IntroduceMe Application/screenshots/6.png)
![Logo](intIntroduceMe Applicationroduceme/screenshots/7.png)
![Logo](IntroduceMe Application/screenshots/8.png)


## Authors

* **Julie Bonnassieux** - *Database Management* - [LinkedIn](https://www.linkedin.com/in/julie-bonnassieux-96576114b/)
* **Romain Velu** - *App’s squelette* - [LinkedIn](https://www.linkedin.com/in/romain-velu-ba7973133/)
* **Alexandros Spartalis** - *NFC Communication* - [LinkedIn](https://www.linkedin.com/in/alexandros-spartalis-901b6313b/)

## License

This project is licensed under the MIT License - see the [LICENSE.md](IntroduceMe Application/LICENSE) file for details




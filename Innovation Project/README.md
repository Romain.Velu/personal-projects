**Diplôme Universitaire Innov’it**

*  Projet dans le cadre de la validation du diplôme DU Innov’it Management des idées dans le secteur du numérique.

Ce diplôme est effectué dans le cadre d’une option au cours de la deuxième année du cursus ingénieur à Télécom Saint Etienne. Il a pour but d’amener des connaissances sur la gestion et le management d’une équipe dans une entreprise, en lien avec le processus d’innovation. Afin d’appliquer les connaissances acquises en cours théoriques, un projet est organisé en lien avec une start-up de l’incubateur de Saint Etienne, afin d’apporter un nouveau point de vue sur l’entreprise. Cela consiste donc à réaliser un business plan complet de l’entreprise, et par la suite proposer un maximum de fonctionnalités et/ou améliorations possibles afin de les intégrer dans le fonctionnement de la startup.
Au cours de ce diplôme, un rapport complet a donc été réalisé en groupe de 4 et une présentation pour expliquer les éléments définis dans ce rapport devant un jury.

________________________________________________________________________________

**University Diploma**

*  Project in the context of the validation of the Diploma Innov'it Management of ideas in the digital sector.

This diploma is carried out as part of an option in the second year of the engineering course at Télécom Saint Etienne. It aims to bring knowledge about the management and management of a team in a company, in connection with the process of innovation. In order to apply the knowledge acquired in theoreticals courses, a project is organized in connection with a startup of the incubator of Saint Etienne, in order to bring a new viewpoint on the company. This is to realize a complete business plan of the company, and thereafter to propose a maximum of features and/or improvements possible to integrate them into the functioning of the startup.
In the course of this diploma, a full report was made in a group of 4 and a presentation explaining the elements defined in this report facing a jury.


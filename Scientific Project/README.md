**Projet Scientifique**


*  Projet réalisé en lien avec une entreprise (Partir.com) dans le cadre d’un projet scientifique en groupe de 6 étudiants réunissant plusieurs disciplines d’une durée de 4 mois. 


*  Le but de ce projet a été de créer de toutes pièces une page web permettant de promouvoir les services de Partir.com de manière ludique et inventive, en s’inspirant du constat de la fausse représentation de la Terre sur un planisphère. 
A partir de là, plusieurs outils ont été conçus afin de mettre en valeur cette déformation, comme le déplacement de pays ou la mesure de distance « réelle » et « fictive » entre deux points. 


*  Ce projet a donc été mené avec un lien étroit avec l’entreprise extérieure à l’école, avec un nouveau point de vue sur la gestion de projet et la mise en œuvre de celui-ci par une équipe. 

Le dossier comprend le rapport final détaillant le processus de développement du site, ainsi que la soutenance. 

Vous pouvez retrouver le site à ce lien :
https://www.partir.com/carte/des-cartes-pour-comprendre-le-monde.html?fbclid=IwAR1EF53ijkxdMalLC3hygYgj127DYpB_IOCVLxucl89F6FrHeXH3eJsZspU

________________________________________________________________________________

**Scientific Project**


*  Project made in connection with a company (Partir.com) in the framework of a scientific project with a group of 6 students with several disciplines of a duration of 4 months. 


*  The aim of this project was to create a Web page to promote the services of Partir.com in a playful and inventive way, drawing on the observation of the false representation of the Earth on a planisphere. 
From there, several tools were designed to enhance this distortion, such as moving countries or measuring "real" and "fictitious" distance between two points. 


*  This project was therefore carried out with a close connection with the company outside the school, with a new viewpoint on the project management and the implementation of it by a team. 

The file includes the final report detailing the development process of the site, as well as the defense. 

You can find the site at this link:
https://www.partir.com/carte/des-cartes-pour-comprendre-le-monde.html?fbclid=IwAR1EF53ijkxdMalLC3hygYgj127DYpB_IOCVLxucl89F6FrHeXH3eJsZspU

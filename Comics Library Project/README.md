**Projet Informatique**

*  Ce projet consiste à réaliser une application permettant aux utilisateurs de classer, chercher, et gérer une bibliothèque virtuelle contenant des BD comics, et accéder aux informations des héros Marvel. 

Celui-ci a été réalisé sur 4 mois, en appliquant une méthode agile, consistant à faire des comptes rendus et des réunions toutes les 2 semaines, correspondant à la durée d’un sprint. Ce projet a été fait avec un groupe de 4 étudiants, avec comme rendu final l’application et une présentation. 
Le dossier comporte les comptes rendus correspondants à chaque sprint, l’exécutable (exécuter la commande java -jar roje.jar) de l’application et la soutenance. 

________________________________________________________________________________

**IT Project**

*  This project consists of realize an application allowing users to classify, search, and manage a virtual library containing comics, and access to the information of Marvel heroes. 

This was done over four months, using an agile method of making reports and meetings every 2 weeks, corresponding to the duration of a sprint. This project was done with a group of 4 students, with as final rendering the application and a presentation. 
The folder contains the corresponding reports for each sprint, the application executable (execute the command java -jar roje.jar) and the defense. 

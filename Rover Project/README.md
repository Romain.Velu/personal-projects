**Projet Rover**

*  Ce projet a été réalisé au sein de l’école Eurecom, durant un semestre avec un groupe de 2 étudiants.

*  Celui-ci a pour but de virtualiser le fonctionnement d’un Rover dans un milieu afin de mieux comprendre les enjeux et les contraintes, et trouver des solutions appropriées.
Plus en détails, le Rover est un engin permettant de circuler en autonomie dans des milieux difficiles voir chaotique, et a pour but de repérer des traces de vie après des évènements tels que des séismes, des tsunamis, ou encore des effondrements d’immeubles. Le Rover a la capacité de naviguer seul, et donc le projet s’est donc orienté sur la mise en place d’une simulation de son comportement suivant le milieu grâce au logiciel TTool. 

Le dossier comporte un rapport détaillé sur le travail effectué, et un fichier xml devant être exécuté grâce à l’outil TTool (disponible à ce site : https://ttool.telecom-paristech.fr/avatar.html). 

________________________________________________________________________________

**Rover Project**

*  This project was carried out within the Eurecom school, during a semester with a group of 2 students.

*  The purpose of this is to virtualize the functioning of a Rover in an environment in order to better understand the stakes and constraints, and find appropriate solutions.
In more detail, the Rover is a vehicle for self-movement in difficult-to-see chaotic environments, and aims to identify traces of life after events such as earthquakes, tsunamis, or building collapses. The Rover has the ability to navigate alone, so the project has focused on setting up a simulation of its behavior according to the environment thanks to the TTool software. 

The folder contains a detailed report of the work performed, and an XML file to be executed using the TTool tool (available at this site: https://ttool.telecom-paristech.fr/avatar.html). 
